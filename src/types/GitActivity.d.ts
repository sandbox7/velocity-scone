export interface IAuthorStats {
  "author": string;
  "activities": Array<IDateActivity>;
}

export interface IDateActivity {
  "date"?: Date;
  "changed_files": number;
  "insertions": number;
  "deletions": number;
}

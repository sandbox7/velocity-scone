import {Command, flags} from '@oclif/command'
import {GitService} from "../services/GitService";
import moment from "moment";

export default class Git extends Command {
  static description = '期間内のdaily commit量を計測します';

  static flags = {
    help: flags.help({char: 'h'}),
    from: flags.string({char: 'f', description: '集計期間from(yyyy-mm-dd)', default: '2019-01-01'}),
    to: flags.string({char: 't', description: '集計期間to(yyyy-mm-dd)', default: '2019-12-31'}),
  };

  static args = [];

  async run() {
    const {args, flags} = this.parse(Git);
    let from = new Date(flags.from), to = new Date(flags.to);

    let logSummary = await GitService.log(from, to);
    let stats = await GitService.statslog(logSummary, from, to);

    stats.forEach((author) => {
      author.activities.forEach((activities) => {
        console.log(`${author.author}\t${moment(activities.date).format('YYYY/MM/DD')}\t${activities.changed_files}\t${activities.insertions}\t${activities.deletions}`)
      })
    })
  }
}

import simplegit from 'simple-git/promise';
import {DefaultLogFields, ListLogSummary} from "simple-git/typings/response";
import moment from "moment";
import {IAuthorStats, IDateActivity} from "../types/GitActivity";

export namespace GitService {
  const git = simplegit();
  const log_opt = {
    '--stat': 4096,
    '--pretty': '%H',
    '--since': '2019-01-01',
    '--until': '2019-12-31',
    '--no-merges': null
  };

  /**
   * 指定期間に発生したcommitとdiffを取得する
   * @param from 集計開始日
   * @param to 集計終了日
   */
  export async function log(from: Date, to: Date): Promise<ListLogSummary<DefaultLogFields>> {
    return git.log(Object.assign(log_opt, {
      '--since': moment(from).format("YYYY-MM-DD"),
      '--until': moment(to).format("YYYY-MM-DD"),
    }));
  }

  /**
   * diff group by author, date を返す
   * @param logSummary commit logのサマリ
   * @param from 集計開始日
   * @param to 集計終了日
   */
  export async function statslog(logSummary: ListLogSummary<DefaultLogFields>, from: Date, to: Date): Promise<Array<IAuthorStats>> {
    let stats: Array<IAuthorStats> = new Array<IAuthorStats>();
    let logList = logSummary.all;

    // repositoryに参加しているauthorを取得
    let authors = logList.map((log) => log.author_name)
      .filter((elem, index, self) => self.indexOf(elem) === index);

    // authorごとに期間内のアクティビティを取得する
    authors.map((author) => {
      let authorStats: IAuthorStats = {
        author: author,
        activities: new Array<IDateActivity>()
      };
      for (let date = from; date < to; date = moment(date).add(1, "day").toDate()) {
        let dateActivity: IDateActivity = logList
          .filter((log) => log.author_name === author
            && moment(new Date(log.date)).format('YYYYMMDD') === moment(date).format('YYYYMMDD'))
          .map((log) => {
            return {
              changed_files: log.diff?.changed,
              insertions: log.diff?.insertions,
              deletions: log.diff?.deletions
            } as IDateActivity;
          })
          .reduce((accumulator, currentValue, currentIndex, array) => {
              console.log(JSON.stringify(accumulator));
              return {
                changed_files: accumulator.changed_files + currentValue.changed_files,
                insertions: accumulator.insertions + currentValue.insertions,
                deletions: accumulator.deletions + currentValue.deletions
              } as IDateActivity
            }, {
              changed_files: 0,
              insertions: 0,
              deletions: 0
            } as IDateActivity
          );
        dateActivity.date = date;
        authorStats.activities.push(dateActivity);
      }
      stats.push(authorStats);
    });

    return stats;
  }
}

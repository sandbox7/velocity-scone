velocity-scone
===
[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
開発速度の評価をします。

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Usage
<!-- usage -->
```bash
git clone https://gitlab.com/sandbox7/velocity-scone
npm install
npm run-script build

cd velocity-scope
npm link
```

<!-- usagestop -->
# Commands
<!-- commands -->
* [`scone backlog [FILE]`](#scone-backlog-file)
* [`scone git [FILE]`](#scone-git-file)
* [`scone help [COMMAND]`](#scone-help-command)

## `scone backlog [FILE]`
`TBU`

_See code: [src\commands\backlog.ts](https://github.com/sandbox/velocity-scone/blob/v0.0.1/src\commands\backlog.ts)_

## `scone git [FILE]`
今いるcloneしたプロジェクトについて、期間内のdaily commit量を計測します

```
USAGE
  $ scone git

OPTIONS
  -f, --from=from  [default: 2019-01-01] 集計期間from(yyyy-mm-dd)
  -h, --help       show CLI help
  -t, --to=to      [default: 2019-12-31] 集計期間to(yyyy-mm-dd)
```

_See code: [src\commands\git.ts](https://github.com/sandbox/velocity-scone/blob/v0.0.1/src\commands\git.ts)_

## `scone help [COMMAND]`

display help for scone

```
USAGE
  $ scone help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src\commands\help.ts)_
<!-- commandsstop -->
